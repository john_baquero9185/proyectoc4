const{ response, request }= require('express');

const usuarioGet =(req = request,res= response)=> {
    const{q,nombre='n.n',apikey,page,limit}=req.query;
    res.json({
        msg: 'Api - Get',
        q,
        nombre,
        apikey,
        page,
        limit
    });
}
const usuarioPost =(req = request,res= response)=> {
    res.json({
        msg: 'Api - Post'
    })
}
const usuarioPut =(req = request,res= response)=> {
    const query =req.query;
    //const {id}= req = request.params;
    res.json({
        msg: 'Api - Put',
        query
    });
}
const usuarioPatch =(req = request,res= response)=> {
    res.json({
        msg: 'Api - Patch'
    })
}
const usuarioDelete =(req = request,res= response)=> {
    res.json({
        msg: 'Api - Delete'
    })
}
module.exports = {
    usuarioGet,
    usuarioPost,
    usuarioPut,
    usuarioPatch,
    usuarioDelete,
}