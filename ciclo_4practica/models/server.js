require('dotenv').config();
const express = require('express')
const cors = require('cors');
const { dbConnection } = require('../database/config');

class Server{
    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.usuarioPath ='/usuario';
        this.conectarDB();
        this.middlewares();

        this.routers();
    }
    async conectarDB(){
        await dbConnection();
    }

    routers(){
        this.app.use(this.usuarioPath, require('../routers/user'))
    }
 
    listen(){
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en el puerto',this.port);
        });
    }
    middlewares(){
        //cors
        this.app.use(cors());
        //directorio publico
        this.app.use(express.static('public'));
    }
}

module.exports =Server;